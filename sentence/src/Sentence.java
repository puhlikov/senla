import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sentence {

    public static void main(String[] args) {

        int count=0;

        String s1="Quantity of words in a sentence";

        for(String s:s1.split(" "))
        {
            count++;
        }
        System.out.println(count);

        List<String> arr = Arrays.asList(s1.split(" "));
        Collections.sort(arr, new Comparator<String>()
        {
            public int compare(String o1, String o2)
            {
                return o1.compareToIgnoreCase(o2);
            }
        });
        System.out.println(arr);

        String[] words = s1.split(" ");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
        }
        s1 = String.join(" ", words);

        System.out.println(s1);
    }
}
