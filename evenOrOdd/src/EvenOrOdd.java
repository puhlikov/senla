
import java.util.Scanner;

public class EvenOrOdd {
    public static void main (String[] args){

        System.out.println("Enter an integer: ");

        Scanner scanner = new Scanner(System.in);

        if (scanner.hasNextInt()){
            if (scanner.nextInt()%2 == 0){
                System.out.println("The number is even");
            } else System.out.println("The number is odd");
        } else System.out.println("Error. Number non-integer");
    }
}
