import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Bag {

    private List<Items> bestItems = null;

    private double maxWeigth;
    private double bestPrice;


    private double totalWeigth(List<Items> items) {
        double totalWeight = 0;

        for (Items item : items) {
            totalWeight += item.getWeigth();
        }
        return totalWeight;
    }


    private double totalPrice(List<Items> items) {
        double totalPrice = 0;

        for (Items item : items) {
            totalPrice += item.getPrice();
        }
        return totalPrice;
    }


    private void checkSetBag (List<Items> items) {
        if (this.bestItems == null) {
            if (this.totalWeigth(items) <= this.maxWeigth) {
                this.bestItems = items;
                this.bestPrice = this.totalPrice(items);
            }
        } else {
            if (this.totalWeigth(items) <= this.maxWeigth && this.totalPrice(items) > this.bestPrice) {
                this.bestItems = items;
                this.bestPrice = this.totalPrice(items);
            }
        }
    }


    public void makeAllSetsBag(List<Items> items) {
        if (items.size() > 0) {
            this.checkSetBag(items);
        }
        for (int i = 0; i < items.size(); i++) {
            List<Items> newSetBag = new ArrayList<>(items);
            newSetBag.remove(i);
            makeAllSetsBag(newSetBag);
        }
    }

    public Bag(double maxWeigth)
    {
        this.maxWeigth = maxWeigth;
    }

    public List<Items> getBestItems() {
        return bestItems;
    }

}
