import java.util.Objects;

public class Items {

    private String name;
    private double weigth;
    private double price;


    public Items (String name, double weigth, double price){
        this.name = name;
        this.weigth = weigth;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeigth() {
        return weigth;
    }

    public void setWeigth(double weigth) {
        this.weigth = weigth;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Items items = (Items) object;
        return Double.compare(items.weigth, weigth) == 0 &&
                Double.compare(items.price, price) == 0 &&
                Objects.equals(name, items.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weigth, price);
    }

    @Override

    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", weigth=" + weigth +
                ", price=" + price +
                '}';
    }
}
