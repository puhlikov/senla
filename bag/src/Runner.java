import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {

        List<Items> items = new ArrayList<>();
        items.add(new Items("Flashlight", 1, 30));
        items.add(new Items("Binoculars", 2, 60));
        items.add(new Items("First-aid kid", 3, 12));
        items.add(new Items("Book", 5, 40));
        items.add(new Items("Water", 4, 5));

        System.out.println(items);

        System.out.println("=================================");

        Bag bag = new Bag(10);
        bag.makeAllSetsBag(items);

        System.out.println(items);

        System.out.println("=================================");

        System.out.println(bag.getBestItems());

    }
}
