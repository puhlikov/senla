import java.io.BufferedReader;
import java.io.InputStreamReader;

public class NodNok {
    public static void main(String[] args){

        int a = 37;
        int b = 5;

        System.out.println(nod(a, b));
        System.out.println(nok(a,b));
    }

    public static int nod(int a, int b) {
        return b == 0 ? a : nod(b, a % b);
        }


    public static int nok(int a, int b) {
        return a / nod(a,b) * b;
    }
}


